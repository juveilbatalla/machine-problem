fun main() {
    var checker = false
    var phoneNumbers: MutableMap<String, String> = mutableMapOf(
        "CJ" to "09115123142",
        "Alyssa" to "09008716312",
        "Kobe" to "09224151232",
        "Ricci" to "09911071641",
        "Cindy" to "09287461626",
        "Francis" to "09125487541",
        "Michael" to "09753524323",
        "Marlon" to "09992837177",
        "Jessi" to "09993719917",
        "Lany" to "09224112314"
    )

    while (!checker) {
        if (phoneNumbers.size <= 30) {
            print("Enter contact name: ")
            var inputName = readLine()!!
            if (inputName != "QUIT") {
                verifyContact(phoneNumbers, inputName)
            } else {
                checker = true
            }
        } else {
            println("Phonebook is already full!")
        }
    }
}
fun verifyContact(contactChecker: MutableMap<String, String>, inputName: String) {
    if (contactChecker.containsKey(inputName)) println("Number: ${contactChecker.getValue(inputName)}")
    else {
        print("Enter contact number: ")
        var input = readLine()!!
        contactChecker[inputName] = input

        println("$inputName is already added on contacts, and  $input is his/her contact number!")
    }
}