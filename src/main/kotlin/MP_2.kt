fun main() {
    var colors = listOf("rED","Pink", "ORANGE","INDIGO","red", "blue", "BLue", "pink","black")
    println(countColor(colors))
}
fun countColor(checkColors: List<Any>): Map<Any, Int> {
    var rainbowColor = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")
    var filteredColor = mutableListOf<Any>()
    checkColors.forEach { color ->
        if (rainbowColor.contains(color.toString().lowercase())) {
            filteredColor.add(color.toString().lowercase())
        } else filteredColor.add("others")
    }
    return filteredColor.groupingBy { it }.eachCount()
}