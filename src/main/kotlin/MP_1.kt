fun main() {
    println(checkingString("Apple"))
    println(checkingString("oRange"))
}

fun checkingString(fruit: String): String {
    return if (fruit.length <= 15) {
        if (fruit.length % 2 == 0) {
            fruit.reversed()
        } else {
            String(fruit.toCharArray().sorted().toCharArray())
        }
    } else
        "Sorry, it can only accept less than or equal to 15 characters!"


}

