fun main() {
    val questions = arrayOf(
        "Question" to "What is your favorite sports?",
        "Choices" to mapOf(
            "A" to "Basketball",
            "B" to "Volleyball",
            "C" to "Lawn Tennis"
        ),
        "Question" to "What is your favorite hobby?",
        "Choices" to mapOf(
            "A" to "Eating",
            "B" to "Jogging",
            "C" to "Gaming"
        ),
        "Question" to "What sports you always watch?",
        "Choices" to mapOf(
            "A" to "Basketball",
            "B" to "Volleyball",
            "C" to "Lawn Tennis"
        ),
        "Question" to "What is your favorite game?",
        "Choices" to mapOf(
            "A" to "Counter Strike",
            "B" to "Valorant",
            "C" to "Dota"
        ),
        "Question" to "What is your favorite color?",
        "Choices" to mapOf(
            "A" to "Black",
            "B" to "Blue",
            "C" to "Red"
        ),
        "Question" to "What is your team in NBA?",
        "Choices" to mapOf(
            "A" to "LA Lakers",
            "B" to "SA Spurs",
            "C" to "GS Warriors"
        ),
        "Question" to "Who is your female favorite athlete?",
        "Choices" to mapOf(
            "A" to "Jaja Santiago",
            "B" to "Pat Patrimonio",
            "C" to "Alyssa Valdez"
        ),
        "Question" to "Who is your team in UAAP?",
        "Choices" to mapOf(
            "A" to "NU Bulldogs",
            "B" to "UST Tigers",
            "C" to "Ateneo Lady Eagles"
        ),
        "Question" to "What is your male favorite athlete?",
        "Choices" to mapOf(
            "A" to "Thirdy Ravena",
            "B" to "Ricci Rivero",
            "C" to "Kobe Paras"
        ),
        "Question" to "What is your favorite number?",
        "Choices" to mapOf(
            "A" to "21",
            "B" to "13",
            "C" to "22"
        ),
    )
    val answer = arrayListOf(
        "B",
        "C",
        "B",
        "B",
        "A",
        "C",
        "C",
        "C",
        "B",
        "C"
    )
    var correctScore = 0
    var counter = 0
    for (i in answer) {
        println("******************************")
        println(questions[counter].second)
        println(questions[counter + 1].second)
        var checker = 0
        while (checker != 1) {
            print("Your answer: ")
            val userAnswer = readLine()!!
            checker = if (userAnswer == "A" || userAnswer == "B" || userAnswer == "C") {
                if (userAnswer == i) {
                    println("*----------Correct!----------*")
                    println("******************************")
                    correctScore++
                } else println("The correct answer is: $i")
                1
            } else 0
        }
        counter += 2
    }

    println("Your total correct answer is: ${correctScore} out of 10 questions")
    println("Out of 10 question you got ${10 - correctScore} incorrect answer")
}